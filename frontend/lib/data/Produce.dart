class Produce {
  final String name;
  final String imagePath;

  Produce(this.name, this.imagePath);
}
