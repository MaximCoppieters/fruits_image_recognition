import 'dart:async';
import 'dart:convert';
import 'dart:io';
// import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
// import 'package:gallery_saver/gallery_saver.dart';
import 'package:path/path.dart';
import 'package:http/http.dart' as http;
import 'package:async/async.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Image Picker Demo',
      home: MyHomePage(title: 'Image Picker Example'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State {
  File _image;

  Future getImageFromCam() async {
    // for camera

    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    // GallerySaver.saveImage(image.path);

    setState(() {
      _image = image;
    });
  }

  Future getImageFromGallery() async {
    // for gallery
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _image = image;
    });
  }

  upload(File imageFile) async {
    print(imageFile.path);

    // open a bytestream
    var stream =
        new http.ByteStream(DelegatingStream.typed(imageFile.openRead()));
    // get file length
    var length = await imageFile.length();

    print(length);

    // string to uri
    var uri = Uri.parse(
        "https://classifyfruitspxltwo.azurewebsites.net/api/classify_image_function");

    // create multipart request
    var request = new http.MultipartRequest("POST", uri);

    // multipart that takes file
    var multipartFile = new http.MultipartFile('file', stream, length,
        filename: basename(imageFile.path));

    // add file to multipart
    request.files.add(multipartFile);

    // send
    var response = await request.send();
    print("hier 1");
    print(response.statusCode);
    print("hier 2");
    // listen for response
    response.stream.transform(utf8.decoder).listen((value) {
      print("...");
      print(value);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // title: Icon(Icons.fastfood),
        backgroundColor: Color.fromRGBO(76, 175, 30, 1),
      ),
      body: ListView(
        children: [
          Container(
            margin: const EdgeInsets.only(bottom: 5.0),
            // width: 300.0,
            height: 400.0,
            child: Center(
              child: _image == null
                  ? Text('No photo selected')
                  : Image.file(_image),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              new SizedBox(
                // width: 150.,
                child: new FlatButton(
                  onPressed: getImageFromCam,
                  color: Color.fromRGBO(76, 175, 30, 1),
                  child: Icon(Icons.camera_alt, color: Colors.white),
                ),
              ),
              new SizedBox(
                // width: 150.0,
                child: new FlatButton(
                  onPressed: getImageFromGallery,
                  color: Color.fromRGBO(76, 175, 30, 1),
                  child: Icon(Icons.photo_library, color: Colors.white),
                ),
              ),
              new SizedBox(
                // width: 150.0,
                child: new FlatButton(
                  onPressed: () => upload(_image),
                  color: Color.fromRGBO(16, 39, 7, 1),
                  child: Icon(Icons.compare, color: Colors.white),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
